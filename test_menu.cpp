/* 
	test_menu.cpp
	Author: M00736336 
	Created: 14/01/2021
	Updated: 22/01/2021 
*/

/*
	Brief: Testing validation and user inputs.
*/
#include "catch.hpp"
#include "Items.h"
#include "program.h"
#include <iostream>
#include <sstream>

// Checks to see if items are being stored.
TEST_CASE("Base Item class can be used to facilitate runtime polymorphism.",
	  "[Item]") {
	std::vector<Item*> items;
	
	Book b(0, "Testing Book", 23, 14.66, 30, 0, 500, "978-3-16-452729-0",
	       "Person P.", 1992);
	Magazine m(1, "Testing Magazine", 40, 12.99, 13, 0, 200, "0022-3840",
		   "TIME", 2001);
	DVD d(2, "Testing DVD", 45, 13.02, 1, 0, 392, 112, "Some Writer",
	      "Some Producer", "Some Genre");
	CD c(3, "Testing CD", 200, 23.99, 12, 10, 1000, 125, "Some Singer", 
	     "Some Album", "Some Genre");

	items.push_back(&b);
	items.push_back(&m);
	items.push_back(&d);
	items.push_back(&c);
	REQUIRE(items.size() == 4);
	REQUIRE(items[0]->category == book);
	REQUIRE(items[1]->category == magazine);
	REQUIRE(items[2]->category == dvd);
	REQUIRE(items[3]->category == cd);
}

// Checks to see if the menu will take an input.
TEST_CASE("Menu", "[menu]"){
	std::streambuf* original_cin = std::cin.rdbuf();
	std::streambuf* original_cout = std::cout.rdbuf();

	std::istringstream input("5\r\n6\r\n0\r\n");
	std::ostringstream output;
	std::cin.rdbuf(input.rdbuf());
	std::cout.rdbuf(output.rdbuf());

	REQUIRE_NOTHROW(menu());

	std::cin.rdbuf(original_cin);
	std::cout.rdbuf(original_cout);
}

// Function to sell an item.
TEST_CASE("A selected item can be sold.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 0;
	int amountSold = 5;
	Magazine m(0, "Testing Magazine", 45, 18.22, 23, 0, 200, "0022-6540",
		   "TIME", 2001);
	items.push_back(&m);
	REQUIRE_NOTHROW(sellItems(items, currentID, amountSold));
}

TEST_CASE("An item that does not exist cannot be sold.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 3;
	int amountSold = 5;
	Magazine m(0, "Testing Magazine", 22, 10.5, 40, 10, 200, "0022-7530",
		   "TIME", 2001);
	items.push_back(&m);
	REQUIRE_NOTHROW(sellItems(items, currentID, amountSold));
}

// Functions to restock an item.
TEST_CASE("A selected item can be restocked.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 0;
	int amountAdded = 5;
	Item* d = new DVD(0, "Testing DVD", 45, 13.02, 1, 0, 392, 112, "Some Writer",
	      "Some Producer", "Some Genre");
	
	items.push_back(d);
	REQUIRE_NOTHROW(restockItems(items, currentID, amountAdded));
 }

TEST_CASE("A selected item that doesn't exist can't be restocked.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 5;
	int amountAdded = 5;
	Item* d = new DVD(0, "Testing DVD", 22, 21.0, 1, 0, 392, 112, "Some Writer",
	      "Some Producer", "Some Genre");

	items.push_back(d);
	REQUIRE_NOTHROW(restockItems(items, currentID, amountAdded));
 }

// Function to see if minimum quantity can be updated.
TEST_CASE("The min quantity of a selected item can be updated", "[Item]") {
	std::vector<Item*> items;
	int currentID = 3;
	int newMin = 0;

	Item* c = new CD(3, "Testing CD", 200, 12.7, 12, 10, 1000, 125, "Some Singer", 
	     "Some Album", "Some Genre");

	items.push_back(c);
	REQUIRE_NOTHROW(updateMinQuantity(items, currentID, newMin));
}

TEST_CASE("Min Quantity is greater than current amount.", "[Item]") {
	int currentID = 3;
	int newMin = 55;

	std::vector<Item*> items;
	Item* c = new CD(3, "Testing CD", 4, 12.7, 12, 10, 1000, 125, "Some Singer", 
	     "Some Album", "Some Genre");

	items.push_back(c);
	REQUIRE_NOTHROW(updateMinQuantity(items, currentID, newMin));
}

// Function to see if maximum quantity can be updated.
TEST_CASE("The max quantity of a selected item can be updated.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 0;
	int newMax = 100;
	Item* c = new CD(0, "Testing CD", 3, 12.7, 1, 10, 10, 125, "Some Singer", 
	     "Some Album", "Some Genre");

	items.push_back(c);
	REQUIRE_NOTHROW(updateMaxQuantity(items, currentID, newMax));
}

TEST_CASE("The max quantity is less than current amount.", "[Item]") {
	std::vector<Item*> items;
	int currentID = 0;
	int newMax = 0;
	Item* c = new CD(0, "Testing CD", 3, 12.7, 1, 10, 10, 125, "Some Singer", 
	     "Some Album", "Some Genre");

	items.push_back(c);
	REQUIRE_NOTHROW(updateMaxQuantity(items, currentID, newMax));
}

// Saves items  
TEST_CASE("Items can be saved to the document.", "[Item]") {
	std::vector<Item*> items;
 	Item* b = new Book(0, "Testing Book", 23, 14.66, 30, 0, 500, "921-452729",
		     "Person P.", 1992);
	items.push_back(b);
	REQUIRE_NOTHROW(saveItems(items));
}

//Following case check to see if adding each an item is possible
TEST_CASE("Add Book", "[add_item]"){
	std::vector<Item*> items;

	std::streambuf* original_cin = std::cin.rdbuf();
	std::streambuf* original_cout = std::cout.rdbuf();

	std::istringstream input("1\r\n\r\n0\r\nTesting Book\r\n0\r\n14.66\r\n0\r\n0\r\n"
				"500\r\n921-452729\r\nPerson P.\r\n1992\r\n");
	std::ostringstream output;
	std::cin.rdbuf(input.rdbuf());
	std::cout.rdbuf(output.rdbuf());

	REQUIRE_NOTHROW(addItems(items));

	std::cin.rdbuf(original_cin);
	std::cout.rdbuf(original_cout);
}
