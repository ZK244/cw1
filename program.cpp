/* 
	program.cpp
	Author: M00736336
	Created: 15/01/2021
	Updated: 21/01/2021
*/


/* 
	This file is created for unit testing.
*/

#include "program.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <string>
#include <sstream>
#include <limits>
#include "Item.h"
#include "Items.h"

template <class T>
void input(std::string const& prompt, T& value) {
	std::string str;
	std::cout << prompt;
	std::getline(std::cin, str);
	std::stringstream vstream(str);
	while (!(vstream >> value && (vstream >> std::ws).eof())) {
		std::cout << "Invalid input! Please try again.\n\n";
		std::cout << prompt;
		std::getline(std::cin, str);
		vstream.str(str);
		vstream.clear();
	}
}

/*
	Overriding the `<<` operator for vectors to provide useful console output.
*/
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vector) {
	os << "[";
	size_t last = vector.size() - 1;
	for (size_t i = 0; i < vector.size(); ++i) {
		os << vector[i];
		if (i != last) os << ", ";
	}
	os << "]";
	return os;
}

std::ostream& operator<<(std::ostream& os, Item* const& item) {
	os << item->to_string();
	return os;
}

/*
	Splits a string by a string delimiter & returns a vector of strings.
*/
std::vector<std::string> split_string(const std::string& str, 
const std::string& delim) {
	std::vector<std::string> result;
	size_t start = 0;
	size_t end = str.find(delim);
	
	while (end != std::string::npos) {
		result.push_back(str.substr(start, end - start));
		start = end + delim.length();
		end = str.find(delim, start);
	}
	result.push_back(str.substr(start, end));
	return result;
}  

/*
	This function is used to parse (read) from the CSV file.
*/
std::vector<Item*> parse(std::istream& stream) {
	std::vector<Item*> result;
	Category current_category;
	std::string line;
	while (std::getline(stream, line)) {
		if (line == "Books:") {
			current_category = book;
		} else if (line == "Magazines:") {
			current_category = magazine;
		} else if (line == "CDs:") {
			current_category = cd;
		} else if (line == "DVDs:") {
			current_category = dvd;
		} else {
			std::vector<std::string> fields = split_string(line, ",");
			if (current_category == book) {
				Item* b = new Book(std::stoi(fields[0]), fields[1], std::stoi(fields[2]), 
						   std::stod(fields[3]), std::stoi(fields[4]), std::stoi(fields[5]),
						   std::stoi(fields[6]), fields[7], fields[8], std::stoi(fields[9]));
				std::cout << "Book Created: " << b->getTitle() << std::endl; 
				result.push_back(b);
    			} else if (current_category == magazine) {
				Item* m =new Magazine(std::stoi(fields[0]), fields[1], std::stoi(fields[2]),
						      std::stod(fields[3]), std::stoi(fields[4]), std::stoi(fields[5]),
						      std::stoi(fields[6]), fields[7], fields[8], std::stoi(fields[9]));
				std::cout << "Magazine Created: " << m->getTitle() << std::endl; 
				result.push_back(m);
			} else if (current_category == dvd) {
				Item* d =new DVD (std::stoi(fields[0]), fields[1], std::stoi(fields[2]), 
						  std::stod(fields[3]),  std::stoi(fields[4]), std::stoi(fields[5]),
						  std::stoi(fields[6]), std::stod(fields[7]), fields[8], fields[9], 
						  fields[10]);
				std::cout << "DVD Created: " << d->getTitle() << std::endl; 
				result.push_back(d);
      
			} else if (current_category == cd) {
				Item* c =new CD(std::stoi(fields[0]), fields[1], std::stoi(fields[2]), 
						std::stod(fields[3]), std::stoi(fields[4]), std::stoi(fields[5]),
						std::stoi(fields[6]), std::stod(fields[7]), fields[8], fields[9], 
						fields[10]);
				std::cout << "CD Created: " << c->getTitle() << std::endl; 
				result.push_back(c);
			}
		} 
	};
	return result;
}

void saveItems(std::vector<Item*>& items){ 
	std::ofstream allData("AllData.csv");

	allData << "Books:" << std::endl;
	for (auto& item : items) { 
		if (item->category == book)
			allData << item << std::endl;
	};
	allData << "Magazines:" << std::endl;
	for (auto& item : items) {
		if (item->category == magazine)
			allData << item << std::endl;
	};
	allData << "DVDs:" << std::endl;
	for (auto& item : items) {
		if (item->category == dvd)
			allData << item << std::endl;
	};
	allData << "CDs:" << std::endl;
	for (auto& item : items) {
		if (item->category == cd)
			allData << item << std::endl;
	};
	allData.close();
}

void loadItems(std::vector<Item*>& items) {
	std::ifstream allData("AllData.csv");
	items = parse(allData);
	allData.close();
}

int itemChecker(std::vector<Item*>& items, int id) {
	bool found_id = false;
	while (!found_id) {
		input("Enter Item ID: ", id);
		for (auto& item : items) {
			if (item->id == id) {
				found_id = true;
			}
		}
		if (found_id) break;
		std::cout << "\nError: ID doesn't exist. Try again: " << std::endl;
	}
	return id;
}

/* 
	The following four functions are called to add a 
	specified item by the user.
*/
void addBooks(std::vector<Item*>& items, int id, std::string title, int amount,
	      double price, int quantitySold, int minQuantity, int maxQuantity){
	std::string ISBN;
	std::string author;
	int year_published;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	input("\nPlease enter the following information:\nItem ID: ", id);
	for (auto& item : items) { 
		while (item->getID() == id){
			input("An item with the same ID already exists! Please try again: ", id);
		}
	};
	std::cout<<"Item Title: ";
	std::getline(std::cin, title);
	input("Current Quantity: ", amount); 
	input("Item Price ($): ", price);  
	input("Amount Sold (if any so far): ", quantitySold);
	input("Minimum Quantity: ", minQuantity);
	while(amount<minQuantity){
 		input("Error! Check your value for minimum requirement!\n"
		      "Enter Minimum Quantity: ", minQuantity);
	}
	input("Maximum Quantity: ", maxQuantity);
	while(amount>maxQuantity || minQuantity>maxQuantity){
		input("Error! Check your value for maximum requirement!\n"
		      "Enter Maximum Quantity: ", maxQuantity);
	}
	std::cout<< "ISBN: "; 
	std::cin>>ISBN;
	std::cin.ignore();
	std::cout<< "Author: "; 
	std::getline(std::cin, author);
	input( "Year Published: ", year_published);
  
	auto book = new Book(id, title, amount, price, quantitySold, minQuantity,
			     maxQuantity, ISBN, author, year_published);
	items.push_back(book);
	std::cout << book << "\n"<< std::endl;
}

void addMagazine(std::vector<Item*>& items, int id, std::string title, int amount,
	      double price, int quantitySold, int minQuantity, int maxQuantity){
	std::string ISBN;
	std::string publisher;
	int year_published;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	input("\nPlease enter the following information:\nItem ID: ", id);
	for (auto& item : items) { 
		while (item->getID() == id){
			input("An item with the same ID already exists! Please try again: ", id);
		}
	};
	std::cout<<"Item Title: ";
	std::getline(std::cin, title);
	input("Current Quantity: ", amount); 
	input("Item Price ($): ", price);  
	input("Amount Sold (if any so far): ", quantitySold);
	input("Minimum Quantity: ", minQuantity);
	while(amount<minQuantity){
 		input("Error! Check your value for minimum requirement!\n"
		      "Enter Minimum Quantity: ", minQuantity);
	}
	input("Maximum Quantity: ", maxQuantity);
	while(amount>maxQuantity || minQuantity>maxQuantity){
		input("Error! Check your value for maximum requirement!\n"
		      "Enter Maximum Quantity: ", maxQuantity);
	}
	std::cout<< "ISBN: "; 
	std::cin>>ISBN;
	std::cin.ignore();
	std::cout<< "Publisher"; 
	std::getline(std::cin, publisher);
	input( "Year Published: ", year_published);
  
	auto magazine = new Magazine(id, title, amount, price, quantitySold,
				     minQuantity, maxQuantity, ISBN, publisher, year_published);
	items.push_back(magazine);
	std::cout << magazine << "\n"<< std::endl;
}

void addDVD(std::vector<Item*>& items, int id, std::string title, int amount,
	    double price, int quantitySold, int minQuantity, int maxQuantity){
	double length; 
	std::string writer;
	std::string producer; 
	std::string genre;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	input("\nPlease enter the following information:\nItem ID: ", id);
	for (auto& item : items) { 
		while (item->getID() == id){
			input("An item with the same ID already exists! Please try again: ", id);
		}
	};
	std::cout<<"Item Title: ";
	std::getline(std::cin, title);
	input("Current Quantity: ", amount); 
	input("Item Price ($): ", price);  
	input("Amount Sold (if any so far): ", quantitySold);
	input("Minimum Quantity: ", minQuantity);
	while(amount<minQuantity){
 		input("Error! Check your value for minimum requirement!\n"
		      "Enter Minimum Quantity: ", minQuantity);
	}
	input("Maximum Quantity: ", maxQuantity);
	while(amount>maxQuantity || minQuantity>maxQuantity){
		input("Error! Check your value for maximum requirement!\n"
		      "Enter Maximum Quantity: ", maxQuantity);
	}
	input("Length (minutes): ", length);
	std::cin.ignore();
	std::cout<< "Writer: "; 
	std::getline(std::cin,writer);
	std::cin.ignore();
	std::cout<< "Producer: "; 
 	std::getline(std::cin,producer);
	std::cin.ignore();
	std::cout<< "Genre: "; 
	std::getline(std::cin,genre);
                
	auto dvd = new DVD (id, title, amount, price, quantitySold, minQuantity,
			    maxQuantity, length, writer, producer, genre);
	items.push_back(dvd);
	std::cout << dvd << "\n"<< std::endl;
}

void addCD(std::vector<Item*>& items, int id, std::string title, int amount, 
	   double price, int quantitySold, int minQuantity, int maxQuantity){
	double length;  
	std::string singer;
	std::string album;   
	std::string genre;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	input("\nPlease enter the following information:\nItem ID: ", id);
	for (auto& item : items) { 
		while (item->getID() == id){
			input("An item with the same ID already exists! Please try again: ", id);
		}
	};
	std::cout<<"Item Title: ";
	std::getline(std::cin, title);
	input("Current Quantity: ", amount); 
	input("Item Price ($): ", price);  
	input("Amount Sold (if any so far): ", quantitySold);
	input("Minimum Quantity: ", minQuantity);
	while(amount<minQuantity){
 		input("Error! Check your value for minimum requirement!\n"
		      "Enter Minimum Quantity: ", minQuantity);
	}
	input("Maximum Quantity: ", maxQuantity);
	while(amount>maxQuantity || minQuantity>maxQuantity){
		input("Error! Check your value for maximum requirement!\n"
		      "Enter Maximum Quantity: ", maxQuantity);
	}
	input("Length (minutes): ", length);
	std::cin.ignore();
	std::cout<< "Singer: "; 
	std::getline(std::cin, singer);
	std::cin.ignore();
	std::cout<< "Album: "; 
	std::getline(std::cin, album);
	std::cin.ignore();
	std::cout<< "Genre: "; 
	std::getline(std::cin, genre);
  
	auto cd = new CD(id, title, amount, price,quantitySold, minQuantity,
			 maxQuantity, length, singer, album, genre);
	items.push_back(cd);
	std::cout << cd << "\n" << std::endl;
}

/*
	This is a function with a Switch-Case that provides the users
	an option of what specific item they want to add.
*/
void addItems(std::vector<Item*>& items){
	int id; 
	std::string title;
	int amount;
	double price;
	int quantitySold;
	int minQuantity;
	int maxQuantity;
	int choice;
	bool run = true;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	while (run!=false){
		input("What item will you be storing?\n1. Book\n2. Magazine\n3. DVD\n4. CD\n"
		      "0. To finish adding more item\n", choice);
		switch (choice) {
			case 1: {
				addBooks(items, id, title, amount, price, quantitySold, minQuantity,
				 	maxQuantity);
				break;
			}
			case 2: {
				addMagazine(items, id, title, amount, price, quantitySold, minQuantity,
				    	    maxQuantity);
        			break;
			}
			case 3: {
				addDVD(items, id, title, amount, price, quantitySold, minQuantity,
			       	       maxQuantity);
				break;
			}
			case 4: {
				addCD(items, id, title, amount, price, quantitySold, minQuantity,
			              maxQuantity);
				break;
			}
			case 0: {
				std::cout<<"Done Adding Items "<<std::endl;
				run = false;
				break;
			}
			default: {
				input("Invalid option! Try again.\n", choice);
			break;
			}
		}
	}
} 

void sellItems(std::vector<Item*>& items, int currentID, int amountSold){
	int result;
  
	for (auto& item : items) {
		if (item->id == currentID) {
			while (item->getAmount()< amountSold){
				std::cout<<"You don't have that many items! Try again:";
			}
  			result = (item->getAmount()) - amountSold ;
			item->setAmount(result);
 			item->setQuantitySold((item->getQuantitySold())+amountSold);
			std::cout << "Amount left is: "<<result << "\n" << item << std::endl;
 		}
	};
}

void restockItems(std::vector<Item*>& items, int currentID, int amountAdded){
	int result;
	for (auto& item : items) { 
		if (item->id == currentID){
			result = (item->getAmount()) + amountAdded ;
			item->setAmount(result);
			std::cout << "New quantity is: "<<result << "\n" << item << std::endl;
		}
	};
}

void updateMinQuantity(std::vector<Item*>& items, int currentID, int newMin){
	for (auto& item : items) { 
		if (item->id == currentID){
			if (newMin>item->getMaxQuantity() || newMin>item->getAmount()){
				std::cout<<"Minimum Quantity has to be smaller than the maximum quantity"
					   " and the current amount you have.";
      			}
      			item->setMinQuantity(newMin);
      			std::cout << "New minimum is: "<<newMin << "\n" << item << std::endl;
    		}
	};
}

void updateMaxQuantity(std::vector<Item*>& items, int currentID, int newMax){
	for (auto& item : items) {  
 		if (item->id == currentID){
			if (newMax<item->getMinQuantity() || newMax<item->getAmount()){
				std::cout<<"Maximum Quantity has to be bigger than the minimum quantity"
					   " and the current amount you have.";
			}
			item->setMaxQuantity(newMax);
			std::cout << "New maximum is: "<<newMax << "\n" << item << std::endl;
		}
	};
}

/*
	This function prints out all the current data that is being stored
	in the vector. 
*/
void currentData(const std::vector<Item*>& items){
	std::cout<<"\n\n***CURRENT DATA***\n"<<std::endl;
	std::cout<<"Number of items: " << items.size() <<std::endl;
	std::cout << "\nBooks:" << std::endl;
	for (auto& item : items) { 
		if (item->category == book)
 			std::cout << item << std::endl;
	};
	std::cout << "\nMagazines:" << std::endl;
	for (auto& item : items) {
		if (item->category == magazine)
			std::cout << item << std::endl;
	};
	std::cout << "\nDVDs:" << std::endl;
	for (auto& item : items) {
		if (item->category == dvd)
			std::cout << item << std::endl;
	};	
	std::cout << "\nCDs:" << std::endl;
	for (auto& item : items) {
	if (item->category == cd)
		std::cout << item << std::endl;
	};
}

/*
	This function displays the information necesarry in the sales report. 
*/
void salesReport(std::vector<Item*>& items){
	std::cout<<"\n\n***SALES REPORT***\n\n"<<std::endl;
	std::cout << "Book:\n";
	for (auto& item : items) { 
		if (item->category == book){
			std::cout << "[ ID: " << item->id << ", Current Quantity: " << item->amount
				  << ", Quantity Sold: " << item->quantitySold << ", Price: " 
				  << item->price << "]" << std::endl;
		}
	};
	std::cout << "\nMagazine:\n";
	for (auto& item : items) { 
		if (item->category == magazine){
			std::cout << "[ ID: " << item->id << ", Current Quantity: " << item->amount
				  << ", Quantity Sold: " << item->quantitySold << ", Price: "
				  << item->price << "]" << std::endl;
		}
	};
	std::cout << "\nDVD:\n";
	for (auto& item : items) { 
		if (item->category == dvd){
			std::cout << "[ ID: " << item->id << ", Current Quantity: " << item->amount
				  << ", Quantity Sold: " << item->quantitySold << ", Price: "
				  << item->price << "]" << std::endl;
		}
	};
	std::cout << "\nCD:\n";
	for (auto& item : items) { 
		if (item->category == cd){
			std::cout << "[ ID: " << item->id << ", Current Quantity: " << item->amount
				  << ", Quantity Sold: " << item->quantitySold << ", Price: "
				  << item->price << "]\n" << std::endl;
		}
	};
}

/*
	This function loops through the items stored in the vector
	and removes the item with the ID that the user inputted. 
*/
void deleteItems(std::vector<Item*>& items){ 
	int currentID;
	char answer;
	std::cout<<"Do you want to delete an item? Y or N: ";
	std::cin>>answer;
	if (answer=='Y'){
		std::cout<<"Enter the ID of the item you want to delete: ";
		std::cin>>currentID;
		for (size_t i = 0; i < items.size(); i++) {
			if (items[i]->id == currentID){ 
				items.erase(items.begin() + i);
				std::cout << "Item has been removed." << std::endl;
			}
		}
	}
	else{
 		std::cout<<"Back to Menu"<<std::endl;
	}
}

/*
	Menu is printed out when the function is called. 
*/
void menuOutput(){
	std::cout << "\n*********************************\n"
                      "1. ADD NEW ITEMS\n"
		      "2. SELL ITEMS\n"	
		      "3. RESTOCK ITEMS\n"
		      "4. UPDATE STOCK QUANTITY\n"
		      "5. VIEW CURRENT DATA\n"
		      "6. VIEW SALES REPORT\n"
		      "7. DELETE AN ITEM\n"
		      "8. LOAD STOCK DATA FROM FILE\n"
		      "9. SAVE STOCK DATA TO FILE\n"
		      "0. QUIT\n"
		      "*********************************\n"
 		      "Enter an option: " << std::endl;
}

/*
	This is a the menu designed as a Switch-Case
	to allow the user to chose the function they want.  
*/
void menu (){
	std::vector<Item*> items;
	int currentID; int amountSold; int amountAdded; int newMax; int newMin; int option;
	bool run = true;

	while (run != false){
		menuOutput();
		std::cin >> option;
		std::cout << "You chose: " << option << "\n" << std::endl;
		switch (option){
			case 1:{
				addItems(items);
				break;
			} case 2:{
				sellItems(items, currentID, amountSold);
				break;
			}case 3:{
 				restockItems(items, currentID, amountAdded);
 				break;
			}case 4:{
				updateMinQuantity(items, currentID, newMin);
 				updateMaxQuantity(items, currentID, newMax);
				break;
			}case 5: {
 				currentData(items);
 				break;
			}case 6:{
				salesReport(items);
				break;
			}case 7:{
				deleteItems(items);
				break;
			}case 8:{
				loadItems(items);
				 break;
			}case 9:{
				saveItems(items);
				std::cout<<"Items have been saved!\n"<<std::endl;
				break;
			} case 0:{
				std::cout<<"Quitting Menu "<<std::endl;
				run = false;
				break;
			}default:{
  				std::cout<<"Invalid option! Try again.\n"<<std::endl;
				std::cin>>option;
				break;
			}
		}
	}
}
