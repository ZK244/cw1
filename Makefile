CXX=g++
CFLAGS=-Wall

#This is a makefile. Author: M00736336

run: all
	./menu

all: menu.cpp
	$(CXX) menu.cpp $(CFLAGS) -o menu

test: test_main.o program.o
	$(CXX) test_main.o program.o test_menu.cpp $(CFLAGS) -o test_menu
	./test_menu

program.o:
	$(CXX) program.cpp $(CFLAGS) -c -o program.o

clean:
	rm -f *.o test_menu

.PHONY: all run clean test
