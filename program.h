#pragma once
/* 
	program.h
	Author: M00736336 
	Created: 14/01/2021
	Updated: 22/01/2021 
*/

#include "Items.h"
#include <vector>

	void menu();
	void sellItems(std::vector<Item*>& items, int currentID, int amountSold);
	void restockItems(std::vector<Item*>& items, int currentID, int amountAdded);
	void updateMinQuantity(std::vector<Item*>& items, int currentID, int newMin);
	void updateMaxQuantity(std::vector<Item*>& items, int currentID, int newMax);
	void saveItems(std::vector<Item*>& items);
	void addItems(std::vector<Item*>& items);
	void addBooks(std::vector<Item*>& items, int id, std::string title,
		      int amount, double price, int quantitySold, int minQuantity,
		      int maxQuantity);
	void addMagazine(std::vector<Item*>& items, int id, std::string title,
			 int amount,double price, int quantitySold, int minQuantity, 
			 int maxQuantity);
	void addDVD(std::vector<Item*>& items, int id, std::string title, int amount,
		    double price, int quantitySold, int minQuantity, int maxQuantity);
	void addCD(std::vector<Item*>& items, int id, std::string title, int amount, 
		   double price, int quantitySold, int minQuantity, int maxQuantity);

