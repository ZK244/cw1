#pragma once
/* 
	Items.h
	Author: M00736336
	Created: 27/12/2020
	Updated: 22/01/2021
*/

/*
	Brief: Below are 4 classes, one for each unique item. They all inherit
	certain data from the Item class, and add on additional data.
*/
#include "Item.h"

class Book : public Item {
public:
	std::string ISBN; 
	std::string author;
	int year_published;
	Book(int id, const std::string& title, int amount, double price, 
	     int quantitySold, int minQuantity, int maxQuantity, 
	     const std::string& ISBN, const std::string& author, int year_published):

	Item(id, title, amount, price, quantitySold, minQuantity, maxQuantity),
	ISBN(ISBN),
	author(author),
	year_published(year_published) {
		this->category = book;
	}
 	std::string getISBN(){
		return ISBN;
	}
	void setISBN(std::string ISBN){
		this->ISBN = ISBN;
	}
	std::string getAuthor(){
		return author;
	}
	void setAuthor(std::string author){
 		this->author = author;
	}
	int getYear_Published(){
 		return year_published;
	}
 	void setYear_Published(int year_published){
		this->year_published = year_published;
	}

	std::string to_string() {
		std::ostringstream output;
		output << id << ',' << title << ',' << amount << ',' << price << ',' 
		       << quantitySold << ',' << minQuantity << ',' << maxQuantity 
		       << ','<< ISBN << ',' << author << ',' << year_published;
		return output.str();
	}
};

class Magazine : public Item {
public:
	std::string ISSN;
	std::string publisher;
	int year_published;

	Magazine(int id, const std::string& title, int amount, double price, 
		 int quantitySold, int minQuantity, int maxQuantity, 
		 const std::string& ISSN, const std::string& publisher, int year_published):

	Item(id, title, amount, price, quantitySold, minQuantity, maxQuantity), 
	ISSN(ISSN),
	publisher(publisher),
	year_published(year_published) {
		this->category = magazine;
	}

	std::string getISSN(){
		return ISSN;
	}
	void setISSN(std::string ISSN){
		this->ISSN = ISSN;
	}
	std::string getPublisher(){
		return publisher;
	}
	void setPublisher(std::string publisher){
		this->publisher = publisher;
	}
	int getYear_Published(){
		return year_published;
	}
	void setYear_Published(int year_published){
		this->year_published = year_published;
	}
	
	std::string to_string() {
		std::ostringstream output;
		output << id << ',' << title << ',' << amount << ',' << price << ','
		       << quantitySold<< ',' << minQuantity << ',' << maxQuantity 
		       << ','<< ISSN << ',' << publisher << ',' << year_published;
		return output.str();
	}
};

class DVD : public Item{
public:
	double length;
	std::string writer;
	std::string producer;
	std::string genre; 

	DVD(int id, const std::string& title, int amount, double price, 
	    int quantitySold, int minQuantity, int maxQuantity, double length,
	    const std::string& writer, const std::string& producer, 
	    const std::string& genre) :

	Item(id, title, amount, price, quantitySold, minQuantity, maxQuantity),
 	length(length),
 	writer(writer),
	producer(producer),
	genre(genre) {
		this->category = dvd;
  	}
	double getLength(){
 		return length;
 	}
	void setLenght(double length){
		this->length = length;
	}
	std::string getWriter(){
		return writer;
	}
 	void setWriter(std::string writer){
		this->writer = writer;
	}
	std::string getProducer(){
		return producer;
	}
	void setProducer(std::string producer){
		this->producer = producer;
	}
	std::string getGenre(){
 		return genre;
	}
	void setGenre(std::string genre){
		this->genre = genre;
	}

	std::string to_string() {
		std::ostringstream output;
		output  << id << ',' << title << ',' << amount << ',' << price << ',' 
			<< quantitySold << ',' << minQuantity << ',' << maxQuantity << ','
			<< length << ',' << writer << ',' << producer << ',' << genre ;
		return output.str();
	}
};

class CD : public Item {
public:
	double length;
	std::string singer;
	std::string album;
	std::string genre; 

	CD(int id, const std::string& title, int amount, double price,
	   int quantitySold, int minQuantity, int maxQuantity, double length,
	   const std::string& singer, const std::string& album, 
	   const std::string& genre) :

	Item(id, title, amount, price, quantitySold, minQuantity, maxQuantity),
	length(length),
	singer(singer),
	album(album),
	genre(genre) {
		this->category = cd;
	}
	double getLength(){
 		return length;
	}
	void setLenght(double length){
		this->length = length;
	}
	std::string getSinger(){
 		return singer;
	}
	void setSinger(std::string singer){
		this->singer = singer;
	}
	std::string getAlbum(){
		return album;
	}
	void setAlbum(std::string album){
		this->album = album;
	}
	std::string getGenre(){
 		return genre;
	}
	void setGenre(std::string genre){
 		this->genre = genre;
	}

 	std::string to_string() {
		std::ostringstream output;
		output  << id << ',' << title << ',' << amount << ',' << price 
			<< ',' << quantitySold << ',' << minQuantity << ','<< maxQuantity 
			<< ',' << length << ',' << singer << ',' << album << ',' << genre ;
		return output.str();
	}
};
