#pragma once 
/* 
	Item.h
	Author: M00736336
	Created: 27/12/2020
	Updated: 21/01/2021
*/

/*
	This is the Item class. Items objects will override from this class
	as a form of inheritance.
*/
#include <string>
#include <sstream>

enum Category{
	book,
	magazine,
	dvd,
	cd
};

class Item {
public:
	int id;
	std::string title;
	int amount;
	double price;
	int quantitySold;
	int minQuantity;
	int maxQuantity;
	Category category;
	
	Item(int id, const std::string& title, int amount, double price, 
	     int quantitySold, int minQuantity, int maxQuantity):

    	id(id), title (title), amount(amount), price(price), 
	quantitySold(quantitySold), minQuantity(minQuantity), 
	maxQuantity(maxQuantity){}

	int getID(){
		return id;
	}
	void setID(int id){
		this->id = id;
	}
	std::string getTitle(){
		return title;
	}
	void setTitle(std::string title){
		this->title = title;
	}
 	int getAmount(){
		return amount;
	}
	void setAmount(int amount){
		this->amount = amount;
	}
	double getPrice(){
 		return price;
 	}
	void setPrice(double price){
		this->price = price;
 	}
 	int getQuantitySold(){
		return quantitySold;
	}
 	void setQuantitySold(int quantitySold){
 		this->quantitySold = quantitySold;
 	}
	int getMinQuantity(){
 		return minQuantity;
	}
 	void setMinQuantity(int minQuantity){
		this->minQuantity = minQuantity;
	}
	int getMaxQuantity(){
		return maxQuantity;
	}
	void setMaxQuantity(int maxQuantity){
		this->maxQuantity = maxQuantity;
	}
	
	virtual std::string to_string(){
		std::ostringstream output;
		output << id << ',' << title << ',' << amount << ',' << price <<  ',' 
		       << quantitySold <<  ',' << minQuantity  << ',' << maxQuantity << ',' ;
		return output.str();
	}
};
